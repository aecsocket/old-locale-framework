# LocaleFramework

Generate different messages depending on a player's locale

---

This plugin allows generating strings depending on a locale, and allowing adding strings to locales using JSON files.

## For server owners

Put the JAR in `$SERVER/plugins` and it will work if any plugins require it.

## For developers

Javadoc is available at [src/main/javadoc](src/main/javadoc).

First, create your JSON files in your JAR as a resource. The format is:

```json
{
  "locale": "en_us",
  "translations": {
    "key": "string"
  }
}
```

Then create (or add to) your `index.json` file in the root of your JAR. The format is:

```json
{
  "lang": [
    [ "file", "path", "dirs", "in", "json", "array" ],
    [ "lang", "en_us.json" ],
    [ "lang", "en_gb.json" ]
  ]
}
```

You can do more advanced functions with these later. In your `Plugin#onEnable`, add:

```java
LocaleFramework.registerLocalesFromIndex(this);
```

Then generate a message using:

```java
LocaleFramework.generate("locale", "key", myArguments);
```

Keys should be formatted as `plugin.module.key`, e.g. `myplugin.coolmodule.money`.

### Features

You are able to:

- Use color codes: use `&x` to change text color and formatting, where `x` is your color code.
- Substitute arguments: use the `String#format` format to place in arguments according to your code
- Substitute other strings in the same locale: use `#{other.locale.key}` to substitute other strings from the same, as
well as escaping this by adding a `\` before the `#`.
locale into the string.

## Download

Currently there is no JAR download available. You must compile it yourself.
