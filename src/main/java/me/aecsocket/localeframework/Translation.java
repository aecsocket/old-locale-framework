package me.aecsocket.localeframework;

import com.google.gson.annotations.SerializedName;
import me.aecsocket.pluginutils.util.Utils;
import org.apache.commons.lang.Validate;

import java.util.HashMap;
import java.util.Map;

/** A link between a key and a string. */
public class Translation {
    /** The locale of this translation. */
    @SerializedName("locale")
    private String locale;
    /** A {@link Map} of strings linked to their keys. */
    @SerializedName("translations")
    private Map<String, String> map;

    public Translation(String locale, Map<String, String> map) {
        this.locale = locale;
        this.map = map;

        Validate.notNull(locale);
        Validate.notNull(map);
    }

    public Translation(String locale) {
        this(locale, new HashMap<>());
    }

    /** Gets the locale of this translation.
     * @return The locale of this translation.
     */
    public String getLocale() { return locale; }

    /** Sets the locale of this translation.
     * @param locale The locale of this translation.
     */
    public void setLocale(String locale) { this.locale = locale; }

    /** Gets a {@link Map} of strings linked to their keys.
     * @return A {@link Map} of strings linked to their keys.
     */
    public Map<String, String> getMap() { return map; }

    /** Gets a string from a key.
     * @param key The key.
     * @return The string.
     */
    public String get(String key) { return map.get(key); }

    /** Adds or updates a string linked to a key.
     * @param key The key.
     * @param string The string.
     */
    public void put(String key, String string) { map.put(key, string); }

    /** Gets if a key exists.
     * @param key The key.
     * @return If a key exists.
     */
    public boolean has(String key) { return map.containsKey(key); }

    /** Generates a string from a key and arguments.
     * @param key The key.
     * @param args An array of arguments to pass to the formatter.
     * @return The string.
     */
    public String generate(String key, Object... args) {
        if (map.containsKey(key))
            return Formatter.format(map.get(key), this, args);
        return null;
    }

    @Override
    public String toString() {
        return Utils.getToString(this);
    }
}
