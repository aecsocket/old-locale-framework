package me.aecsocket.localeframework.exception;

public class LocaleException extends RuntimeException {
    public LocaleException(String message) {
        super(message);
    }
}
