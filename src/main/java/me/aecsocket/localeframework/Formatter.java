package me.aecsocket.localeframework;

import me.aecsocket.localeframework.exception.LocaleException;
import me.aecsocket.pluginutils.util.Utils;
import org.bukkit.ChatColor;

import java.util.MissingFormatArgumentException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** A class for formatting generated strings. */
public final class Formatter {
    /** The character to use in {@link ChatColor#translateAlternateColorCodes}. */
    public static final char ALT_COLOR_CHAR = '&';
    /** The {@link Pattern} used in {@link Formatter#formatSubstitutes}. */
    public static final Pattern SUBSTITUTE_PATTERN = Pattern.compile("(?<!\\\\)#\\{(.+?)}");

    /** Substitutes arguments into a string.
     * @param string The string.
     * @param args The arguments to substitute.
     * @return The substituted string.
     */
    public static String formatArgs(String string, Object... args) {
        try {
            return String.format(string, args);
        } catch (MissingFormatArgumentException e) {
            throw new LocaleException(String.format("Failed to format arguments for '%s' (%s)\n%s", string, Utils.arrayToString(args), e));
        }
    }

    /** Uses {@link ChatColor#translateAlternateColorCodes} to modify a string.
     * @param string The string to modify.
     * @return The modified string.
     */
    public static String formatColor(String string) {
        return ChatColor.translateAlternateColorCodes(ALT_COLOR_CHAR, string);
    }

    /** Substitutes other strings from a {@link Translation} into a string.
     * @param string The string.
     * @param translation The translation to substitute from.
     * @return The substituted string.
     */
    public static String formatSubstitutes(String string, Translation translation) {
        Matcher match = SUBSTITUTE_PATTERN.matcher(string);

        int i = 0;
        while (match.find()) {
            if (match.groupCount() < 1)
                continue;

            String substitute = match.group(1);
            String generated = translation.generate(substitute);
            if (generated == null)
                continue;
            string = match.replaceFirst(generated);
            match = SUBSTITUTE_PATTERN.matcher(string);
        }

        return string;
    }

    /** Formats a string using all the methods.
     * @param string The string to format.
     * @param translation The translation for {@link Formatter#formatSubstitutes(String, Translation)}
     * @param args The arguments for {@link Formatter#formatArgs(String, Object...)}
     * @return The formatted string.
     */
    public static String format(String string, Translation translation, Object... args) {
        return formatArgs(formatSubstitutes(formatColor(string), translation), args);
    }
}
