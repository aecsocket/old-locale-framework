package me.aecsocket.localeframework;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.aecsocket.localeframework.exception.LocaleException;
import me.aecsocket.pluginutils.util.Utils;
import me.aecsocket.resourceframework.ResourceFramework;
import me.aecsocket.resourceframework.index.Index;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static me.aecsocket.pluginutils.PluginUtils.getConfigValue;

/** The main plugin class. */
public class LocaleFramework extends JavaPlugin {
    /** The name of the resource key in the index file. */
    public static final String RESOURCE_KEY = "lang";

    /** An instance of this plugin. */
    private static LocaleFramework instance;
    /** How verbose this plugin is. See {@link Verbosity} for a full list. */
    private static int verbosity;
    /** The default-locale value in the config. */
    private static String defaultLocale;
    /** The default-text value in the config. */
    private static String defaultText;
    /** A {@link Translation} based on the {@link LocaleFramework#defaultLocale}. */
    private static Translation defaultTranslation;
    /** The {@link Gson} this uses to convert JSON. */
    private static Gson gson;

    /** A {@link Map} of {@link Translation}s linked to their locales. */
    private static Map<String, Translation> translations;

    /** Gets an instance of this plugin.
     * @return An instance of this plugin.
     */
    public static LocaleFramework getInstance() { return instance; }

    /** Gets how verbose this plugin is. See {@link Verbosity} for a full list.
     * @return How verbose this plugin is. See {@link Verbosity} for a full list.
     */
    public static int getVerbosity() { return verbosity; }

    /** Gets the default-locale value in the config.
     * @return The default-locale value in the config.
     */
    public static String getDefaultLocale() { return defaultLocale; }

    /** Gets the default-text value in the config.
     * @return The default-text value in the config.
     */
    public static String defaultTextOrException() { return defaultText; }

    /** Gets a {@link Translation} based on the {@link LocaleFramework#defaultLocale}.
     * @return A {@link Translation} based on the {@link LocaleFramework#defaultLocale}.
     */
    public static Translation getDefaultTranslation() { return defaultTranslation; }

    /** Gets a {@link Map} of {@link Translation}s linked to their locales.
     * @return A {@link Map} of {@link Translation}s linked to their locales.
     */
    public static Map<String, Translation> getTranslations() { return translations; }

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        verbosity = getConfigValue(this, "verbosity", Integer.class);
        defaultLocale = getConfigValue(this, "default-locale", String.class);
        defaultText = getConfigValue(this, "default-text", String.class);
        defaultTranslation = null;
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        translations = new HashMap<>();
    }

    /** Returns either {@link LocaleFramework#defaultText} or throws a {@link RuntimeException} depending on if
     * {@link LocaleFramework#defaultText} is null or not.
     * @param exception The {@link RuntimeException} to throw.
     * @return {@link LocaleFramework#defaultText}.
     */
    public static String defaultTextOrException(RuntimeException exception) {
        if (defaultText == null)
            throw exception;
        return defaultText;
    }

    /** Returns {@link LocaleFramework#defaultText} and logs a warning.
     * @param prefix The log prefix.
     * @param lines The lines of text to log.
     * @return {@link LocaleFramework#defaultText}.
     */
    public static String defaultTextAndWarn(String prefix, String... lines) {
        log(1, prefix, lines);
        return defaultText;
    }

    /** Logs text as this plugin.
     * @param level What level of verbosity is required for this.
     * @param prefix The log prefix.
     * @param lines The lines of text to log.
     */
    public static void log(int level, String prefix, String... lines) {
        if (verbosity >= level) {
            for (String line : lines)
                System.out.println(String.format("[%s] %s: %s", instance.getName(), prefix, line));
        }
    }

    /** Registers all translation files from a {@link Plugin} and an {@link Index}.
     * @param plugin The {@link Plugin}.
     * @param index The {@link Index}.
     */
    public static void registerLocalesFromIndex(Plugin plugin, Index index) {
        log(2, "REGISTER", String.format("%s: %s", plugin.getName(), index));

        if (index.has(RESOURCE_KEY)) {
            List<String> resources = index.get(RESOURCE_KEY).getStringResources(plugin);

            for (String resource : resources) {
                Translation translation = gson.fromJson(resource, Translation.class);
                log(2, "REGISTER", translation.toString());
                String locale = translation.getLocale();
                if (translations.containsKey(locale))
                    translations.get(locale).getMap().putAll(translation.getMap());
                else
                    translations.put(locale, translation);
            }
        }
    }

    /** Registers all translation files from a {@link Plugin}'s {@link Index}.
     * @param plugin The {@link Plugin}.
     */
    public static void registerLocalesFromIndex(Plugin plugin) { registerLocalesFromIndex(plugin, ResourceFramework.getIndex(plugin)); }

    /** Generates a string based on the locale, key and arguments.
     * @param locale The locale.
     * @param key The string key.
     * @param args An array of arguments to pass to the formatter.
     * @return The generated string.
     */
    public static String generate(String locale, String key, Object... args) {
        log(3, "GENERATE", String.format("%s:%s (%s)", locale, key, Utils.arrayToString(args)));

        if (translations.containsKey(locale) && translations.get(locale) != null) {
            Translation translation = translations.get(locale);
            log(3, "GENERATE", String.format("Found translation %s", translation));
            if (translation.has(key)) {
                String generated = translation.generate(key, args);
                log(3, "GENERATE", String.format("Generated '%s'", generated));

                if (generated != null) {
                    log(3, "GENERATE", String.format("Returned '%s'", generated));
                    return generated;
                }
            }
            log(3, "GENERATE", "Translation did not have key");
        } else {
            log(3, "GENERATE", String.format("Translations did not contain locale %s", locale));
            if (locale.equals(defaultLocale)) {
                log(3, "GENERATE", String.format("Returned '%s'", defaultText));
                return defaultTextAndWarn("GENERATE", String.format("No default locale '%s' found", locale));
            }
        }

        if (locale.equals(defaultLocale))
            return defaultTextOrException(new LocaleException(String.format("No text for '%s' found", key)));
        log(3, "GENERATE", "Generated is null, trying fallback");
        return generate(defaultLocale, key, args);
    }

    /** Generates a string with the default locale based on a key and arguments.
     * @param key The string key.
     * @param args An array of arguments to pass to the formatter.
     * @return The generated string.
     */
    public static String generate(String key, Object... args) { return generate(defaultLocale, key, args); }
}
