package me.aecsocket.localeframework;

/** Defines how verbose the plugin is. */
public final class Verbosity {
    /** No messages will be logged. */
    public static final int SILENT = 0;
    /** Only warnings will be logged. */
    public static final int WARNING = 1;
    /** Plugins registering translations will be logged. */
    public static final int REGISTER = 2;
    /** Individual #generate calls will be logged. */
    public static final int GENERATE = 3;
}
